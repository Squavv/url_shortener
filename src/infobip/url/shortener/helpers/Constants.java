package infobip.url.shortener.helpers;

public class Constants {
	public static final String SHORT_URL_BASE = "http://short.com/";
	public static final int DEFAULT_SHORT_URL_EXT_LENGTH = 6;
	public static final int DEFAULT_REDIRECT_TYPE = 302;
	public static final String HELP_FILE_NAME = "help.pdf";
	
	// Error messages
	public static final String ERR_TO_JSON = "Unable to serialize given object to JSON.";
	public static final String ERR_NO_RIGHTS = "Nemas prava.";
	public static final String ERR_URL_REQUIRED = "Url must be provided!";
	public static final String ERR_ACCOUNT_ID_REQUIRED = "Account id must be provided!";
}
