package infobip.url.shortener.helpers;

import infobip.url.shortener.models.ShortUrl;
import infobip.url.shortener.models.Url;
import infobip.url.shortener.models.UserAccount;

import java.io.IOException;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

public class Converter {

	/*
	 * Serializes RegistrationResponse to json
	 */
	public static String toJson(Object response) {
		ObjectMapper mapper = new ObjectMapper();
		String result = Constants.ERR_TO_JSON;
		try {
			result = mapper.writeValueAsString(response);
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	/*
	 * Serializes Url to json and returns shortUrl as json
	 */
	public static String toJson(Url response) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String result = mapper.writeValueAsString(response);
			JsonNode json = mapper.readTree(result);
			return json.get("shortUrl").toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/*
	 * Deserializes given json to UserAccount object
	 */
	public static UserAccount toUserAccountObject(String request) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			UserAccount user = mapper.readValue(request, UserAccount.class);
			return user;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * Deserializes given json to Url object
	 */
	public static Url toUrlObject(String request) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			Url url = mapper.readValue(request, Url.class);
			return url;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * Deserializes given json to ShortUrl object
	 */
	public static ShortUrl toShortUrlObject(String request) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			ShortUrl shortUrl = mapper.readValue(request, ShortUrl.class);
			return shortUrl;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
