package infobip.url.shortener.helpers;

import java.security.SecureRandom;

public class HelperMethods {
	
	/*
	 * @param int passwordLength
	 * Generates and returns alfanumeric password 
	 * of desired length
	 */
	public static String generatePassword(int passwordLength) {
		SecureRandom rnd = new SecureRandom();
		final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		StringBuilder sb = new StringBuilder(passwordLength);
		for (int i = 0; i < passwordLength; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}
}
