package infobip.url.shortener.services;

import infobip.url.shortener.helpers.Constants;
import infobip.url.shortener.helpers.Converter;
import infobip.url.shortener.models.ShortUrl;
import infobip.url.shortener.models.Url;
import infobip.url.shortener.models.UrlList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

@Path("/register")
public class ShortenerService {
	UrlList ul = UrlList.getInstance();

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Object registerUrl(@Context HttpHeaders header, String request) {
		Url url = null;
		String accountId = AuthenticationService.authenticateUser(header);
		if (!accountId.isEmpty()) {
			url = Converter.toUrlObject(request);
			Object result = (url.getUrl() != null) ? Converter.toJson(ul.addUrl(url,
					ShortUrl
							.shortUrl(Constants.DEFAULT_SHORT_URL_EXT_LENGTH),
					accountId)) : Converter.toJson(Constants.ERR_URL_REQUIRED);
					return result;
		} else {
			return Constants.ERR_NO_RIGHTS;
		}
	}
}