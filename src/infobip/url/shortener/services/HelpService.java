package infobip.url.shortener.services;

import infobip.url.shortener.helpers.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/help")
public class HelpService {

    @GET
    public Response getHelp() throws FileNotFoundException {
        URL url = getClass().getResource(Constants.HELP_FILE_NAME);
        File file = new File(url.getPath());
        FileInputStream fileInputStream = new FileInputStream(file);
        Response.ResponseBuilder responseBuilder = Response.ok(fileInputStream);
        responseBuilder.type("application/pdf");
        return responseBuilder.build();
    }
    
}
