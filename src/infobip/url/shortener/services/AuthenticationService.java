package infobip.url.shortener.services;

import infobip.url.shortener.models.UserList;

import java.nio.charset.Charset;
import java.util.Base64;

import javax.ws.rs.core.HttpHeaders;

public class AuthenticationService {
	public static String authenticateUser(HttpHeaders header) {
		String username = null;
		String password = null;
		String authHeader = header.getRequestHeader(HttpHeaders.AUTHORIZATION)
				.get(0);
		if (authHeader != null && authHeader.startsWith("Basic")) {
			String base64Credentials = authHeader.substring("Basic".length())
					.trim();
			String credentials = new String(Base64.getDecoder().decode(
					base64Credentials), Charset.forName("UTF-8"));
			username = credentials.split(":")[0];
			password = credentials.split(":")[1];
		}
		
		return (UserList.authenticationSuccessfull(username, password));
	}
}
