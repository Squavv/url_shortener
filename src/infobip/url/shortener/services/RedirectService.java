package infobip.url.shortener.services;

import java.net.URISyntaxException;

import infobip.url.shortener.helpers.Converter;
import infobip.url.shortener.models.ShortUrl;
import infobip.url.shortener.models.Url;
import infobip.url.shortener.models.UrlList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/goto")
public class RedirectService {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerUrl(String request) {
		UrlList ul = UrlList.getInstance();
		ShortUrl shortUrl = Converter.toShortUrlObject(request);
		Url url = ul.getUrlByShortUrl(shortUrl);
		java.net.URI location;
		try {
			location = new java.net.URI(url.getUrl());
			url.addVisit();
			return Response.temporaryRedirect(location).build();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}
}
