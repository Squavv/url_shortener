package infobip.url.shortener.services;

import infobip.url.shortener.helpers.Constants;
import infobip.url.shortener.helpers.Converter;
import infobip.url.shortener.models.RegistrationResponse;
import infobip.url.shortener.models.UserAccount;
import infobip.url.shortener.models.UserList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/account")
public class UserRegistrationService {

	UserList ul = UserList.getInstance();

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String createUser(String accountId) {
		UserAccount user = null;
		try {
			user = Converter.toUserAccountObject(accountId);
			return ul.addUser(user);
		} catch (NullPointerException e) {
			return Converter.toJson(new RegistrationResponse(Constants.ERR_ACCOUNT_ID_REQUIRED));
		}
	}
}
