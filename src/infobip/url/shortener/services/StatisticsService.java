package infobip.url.shortener.services;

import infobip.url.shortener.helpers.Constants;
import infobip.url.shortener.models.UrlList;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

import org.codehaus.jackson.map.ObjectMapper;

@Path("/statistic")
public class StatisticsService {
	UrlList ul = UrlList.getInstance();
	
	@GET
	public Object getStatistics(@Context HttpHeaders header, String request) {
		String accountId = AuthenticationService.authenticateUser(header);
		if (accountId != "") {
			try {
				return new ObjectMapper().writeValueAsString(ul.fillMapForStatistics(accountId));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			return Constants.ERR_NO_RIGHTS;
		}
		return null;
	}

}
