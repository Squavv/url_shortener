package infobip.url.shortener.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UrlList {
	private static UrlList urls = new UrlList();
	static ArrayList<Url> urlList = new ArrayList<>();

	private UrlList() {
	}

	public static UrlList getInstance() {
		return urls;
	}

	/*
	 * Updates url's data and adds url to the list.
	 */
	public Url addUrl(Url url, ShortUrl shortUrl, String accountId) {
		Url link = notContains(url);
		if (link != null) {
			link.setRedirectType(url.getRedirectType());
			link.setAccountId(accountId);
			return (link);
		} else {
			url.setShortUrl(shortUrl);
			url.setRedirectType(url.getRedirectType());
			url.setAccountId(accountId);
			urlList.add(url);
			return (url);
		}
	}

	/*
	 * Checks if there is given url in the list
	 */
	private Url notContains(Url url) {
		for (Url link : urlList) {
			if (link.getUrl().equals(url.getUrl())) {
				return link;
			}
		}
		return null;
	}

	/*
	 * Finds and returns Url by given shortUrl
	 */
	public Url getUrlByShortUrl(ShortUrl shortUrl) {
		for (Url link : urlList) {
			if (link.getShortUrl().getShortUrl().equals(shortUrl.getShortUrl())) {
				return link;
			}
		}
		return null;
	}

	public Map<String, String> fillMapForStatistics(String accountId) {
		Map<String, String> map = new HashMap<>();
		for (Url url : urlList) {
			if (url.getAccountId().equals(accountId))
				map.put(url.getUrl(), String.valueOf(url.getVisitNumber()));
		}
		return map;
	}
}
