package infobip.url.shortener.models;

public class RegistrationResponse {
	private boolean success;
	private String description;
	private String password;
	
	
	/**
	 * An empty constructor
	 */
	public RegistrationResponse() {
		super();
	}
	
	/**
	 * Constructor for an error message
	 */
	public RegistrationResponse(String decsription) {
		this.description = decsription;
	}

	/**
	 * @param success
	 * @param description
	 * @param password
	 */
	public RegistrationResponse(boolean success, String description,
			String password) {
		super();
		this.success = success;
		this.description = description;
		this.password = password;
	}


	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	@Override
	public String toString() {
		return "RegistrationResponse [success=" + success + ", description="
				+ description + ", password=" + password + "]";
	}
	
}
