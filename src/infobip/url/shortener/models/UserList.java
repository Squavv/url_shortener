package infobip.url.shortener.models;

import infobip.url.shortener.helpers.Converter;

import java.util.ArrayList;

public class UserList {
	private static UserList users = new UserList();
	static ArrayList<UserAccount> userList = new ArrayList<>();

	private UserList() {
	}

	public static UserList getInstance() {
		return users;
	}

	/*
	 * Updates user's password and 
	 * adds user to the list.
	 */
	public String addUser(UserAccount user) {
		if (notContains(userList, user)) {
			user.setPassword();
			userList.add(user);
			RegistrationResponse registrationResponse = new RegistrationResponse(
					true, "Your account is opened.", user.getPassword());
			return (Converter.toJson(registrationResponse));
		} else {
			RegistrationResponse registrationResponse = new RegistrationResponse(
					false, "Your account already exists.", "");
			return (Converter.toJson(registrationResponse));
		}
	}

	/*
	 * Checks if there is a user in list with this accountId
	 */
	private boolean notContains(ArrayList<UserAccount> users,
			UserAccount userAcc) {
		boolean notContains = true;
		for (UserAccount user : users) {
			if (user.getAccountId().equals(userAcc.getAccountId())) {
				notContains = false;
				break;
			}
		}
		return notContains;
	}

	/*
	 * Check if provided credentials are valid
	 */
	public static String authenticationSuccessfull(String userAccountId,
			String userPassword) {
		String authenticationSuccessfull = "";
		for (UserAccount user : userList) {
			if (user.getAccountId().equals(userAccountId)
					&& user.getPassword().equals(userPassword)) {
				authenticationSuccessfull = user.getAccountId();
				break;
			}
		}
		return authenticationSuccessfull;
	}

	@Override
	public String toString() {
		return "UserList: " + userList.toString();
	}

}
