package infobip.url.shortener.models;

import infobip.url.shortener.helpers.HelperMethods;

public class UserAccount {
	private static final int PASSWORD_LENGTH = 8;
	private String accountId;
	private String password;

	/**
	 * An empty constructor
	 */
	public UserAccount() {
		super();
	}

	/**
	 * @param accountId
	 */
	public UserAccount(String accountId) {
		super();
		this.accountId = accountId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getPassword() {
		return password;
	}

	/**
	 * Generates a new password for user, and sets it
	 */
	public void setPassword() {
		this.password = HelperMethods.generatePassword(PASSWORD_LENGTH);
	}

	@Override
	public String toString() {
		return "UserAccount [accountId=" + accountId + ", password=" + password
				+ "]";
	}

}
