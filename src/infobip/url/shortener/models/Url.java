package infobip.url.shortener.models;

import infobip.url.shortener.helpers.Constants;

public class Url {
	private String url;
	private ShortUrl shortUrl;
	private int redirectType;
	private int visitNumber;
	private String accountId;

	/**
	 * An empty constructor
	 */
	public Url() {
		super();
	}

	/**
	 * @param url
	 * @param redirectType
	 */
	public Url(String url, int redirectType) {
		super();
		this.url = url;
		this.redirectType = redirectType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ShortUrl getShortUrl() {
		return shortUrl;
	}

	public void setShortUrl(ShortUrl shortUrl) {
		this.shortUrl = shortUrl;
	}

	public int getRedirectType() {
		return redirectType;
	}

	public void setRedirectType(int redirectType) {
		this.redirectType = (redirectType == 0) ? Constants.DEFAULT_REDIRECT_TYPE
				: redirectType;
	}

	public int getVisitNumber() {
		return visitNumber;
	}

	public void setVisitNumber(int visitNumber) {
		this.visitNumber = visitNumber;
	}
	
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public void addVisit() {
		this.setVisitNumber(this.getVisitNumber() + 1);
	}

	@Override
	public String toString() {
		return "Url [url=" + url + ", shortUrl=" + shortUrl + ", redirectType="
				+ redirectType + ", visitNumber=" + visitNumber + "]";
	}
}
