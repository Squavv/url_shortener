package infobip.url.shortener.models;

import infobip.url.shortener.helpers.Constants;
import infobip.url.shortener.helpers.HelperMethods;

public class ShortUrl {
	private String shortUrl;

	/**
	 * An empty constructor
	 */
	public ShortUrl() {
		super();
	}

	/**
	 * @param shortUrl
	 */
	public ShortUrl(String shortUrl) {
		super();
		this.shortUrl = shortUrl;
	}

	/**
	 * @return the shortUrl
	 */
	public String getShortUrl() {
		return shortUrl;
	}

	/**
	 * @param shortUrl the shortUrl to set
	 */
	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}

	/*
	 * @param int length of desired extension
	 * Creates and returns short url
	 */
	public static ShortUrl shortUrl(int extLength) {
		return new ShortUrl(Constants.SHORT_URL_BASE + HelperMethods.generatePassword(extLength));
	}
	
	@Override
	public String toString() {
		return "ShortUrl [shortUrl=" + shortUrl + "]";
	}
	
	
	
}
